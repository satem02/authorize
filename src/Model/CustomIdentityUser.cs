using Microsoft.AspNetCore.Identity;

namespace src.Model
{
    public class CustomIdentityUser : IdentityUser
    {
        public CustomIdentityUser()
        {
        }

        public CustomIdentityUser(string userName) : base(userName)
        {
        }
    }
}