namespace src.Model
{
    public class LoginViewModel
    {
        public string Mail { get; set; }
        public string Password { get; set; }
    }
}