using System.Security.Claims;
using src.Model;

namespace src.Services.Abstract
{
    public interface IUserSessionService
    {
        string GetPhoneNumber();
        string GetRole();
        string GetEmail();
        string GetUserName();
        string GetUserId();
        Claim[] GetClaims(CustomIdentityUser user);
    }
}